module codeberg.org/apapsch/go-streaming-channel

go 1.18

require (
	codeberg.org/apapsch/go-aggregate-error v0.1.1
	github.com/stretchr/testify v1.8.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
