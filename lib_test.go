package streamchan

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/require"
)

var errTest = errors.New("very, much bad")

func TestIterateStream(t *testing.T) {
	must := require.New(t)
	ctx := context.Background()
	no := 0

	err := DrainStream(
		ctx,
		func(ctx context.Context, stream chan string) error {
			stream <- "foo"
			close(stream)
			return nil
		},
		func(ctx context.Context, it string) error {
			no++
			must.Equal("foo", it)
			return nil
		},
	)
	must.Nil(err)
	must.Equal(1, no)
}

func TestConsumerError(t *testing.T) {
	must := require.New(t)
	ctx := context.Background()
	err := DrainStream(
		ctx,
		func(ctx context.Context, stream chan string) error {
			close(stream)
			return errTest
		},
		func(ctx context.Context, entity string) error {
			return nil
		},
	)
	must.NotNil(err)
	must.Equal("failed draining stream: very, much bad", err.Error())
}

func TestProducerError(t *testing.T) {
	must := require.New(t)
	ctx := context.Background()
	err := DrainStream(
		ctx,
		func(ctx context.Context, stream chan string) error {
			stream <- "foo"
			close(stream)
			return nil
		},
		func(ctx context.Context, entity string) error {
			return errTest
		},
	)
	must.NotNil(err)
	must.Equal("failed draining stream: very, much bad", err.Error())
}

func TestFinisher(t *testing.T) {
	must := require.New(t)
	ctx := context.Background()
	no := 0
	err := DrainStream(
		ctx,
		func(ctx context.Context, stream chan string) error {
			stream <- "foo"
			close(stream)
			return nil
		},
		func(ctx context.Context, entity string) error {
			return nil
		},
		func(ctx context.Context) error {
			no++
			return nil
		},
	)
	must.Nil(err)
	must.Equal(1, no)
}
