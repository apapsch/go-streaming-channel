# go-streaming-channel

DRY by encapsulating channel draining logic (producing and consuming items on a
channel). Thanks to Go 1.18 generics it is type safe. See the tests for
examples.
